#! /bin/bash

#check the argument and throw a msg if it fails:
if [ $# -lt 1 ]; then
  echo "Usage: auto_rm [PATH] [TIMER] -h"
  exit 1
fi
# parse cmd line:
for i in $@; do
  #add readme authors and todo
  if [ $i = "-h" ]; then
    #display usage:
    echo "Usage: auto_rm [DATE] [PATH] [TIMER] -h"
    echo "PATH  : repository to scan"
    echo "TIMER : in secondes!"
    exit 0
  fi
done

#constant here:
sc=$1
timer=$2
n=0

#main timer loop:
while [ true ]; do
  #delete all dirty files
  if [ "$(find $sc -type f -name ".*" -exec rm -fiv {} \;)" != "" ]; then
    n=1
  fi

  if [ "$(find $sc -type f -name "*.sw*" -exec rm -fiv {} \;)" != "" ]; then
    n=1
  fi

  if [ "$(find $sc -type f -name "*.*~" -exec rm -fiv {} \;)" != "" ]; then
    n=1
  fi

  if [ "$(find $sc -type f -name "test" -exec rm -fiv {} \;)" != "" ]; then
    n=1
  fi

  if [ $n -eq 1 ]; then
    tree -a $sc
  fi
  sleep $timer
  #re-init n:
  n=0
done
echo "the time is out, program will exit now..."
